# Flathub/Flatpak

Flatpak ist ein universales Paketformat, welches im Benutzer verwendet werden kann.

Flathub ist die größte Quelle von Paketen für Flatpak, um sie im Nutzer hinzuzufügen, einfach den folgenden Command eingeben.

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo --user
```

Darauf könnt ihr in der Software App, verfügbare Apps installieren.

![screenshot1.png](./screenshot1.png)

Allerding sollte man aufpassen das Flathub (Flatpak) und nicht rpm oder 

Fedora (Flatpak) ausgewählt ist.

![screenshot4.png](./screenshot4.png)

![screenshot2.png](./screenshot2.png)

<img title="" src="./screenshot3.png" alt="screenshot3.png" width="227">

Wenn die richtige Quelle ausgewählt ist, könnt ihr die Applikation installieren
